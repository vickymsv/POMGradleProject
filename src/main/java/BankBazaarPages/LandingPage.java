package BankBazaarPages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;


import wdMethods.SeMethods;

public class LandingPage extends SeMethods {

	public MutualFundPage clickMutualFund() {
	Actions builder = new Actions(driver);
	
	builder.moveToElement(locateElement("xpath", "//a[@href='/investment.html']")).perform();
	
	WebElement eleclickmutualfund= locateElement("xpath", "//a[@href='/mutual-fund.html']");
	click(eleclickmutualfund);
		return new MutualFundPage();
}
}
