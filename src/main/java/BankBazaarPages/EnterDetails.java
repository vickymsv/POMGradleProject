package BankBazaarPages;

import org.openqa.selenium.WebElement;

import wdMethods.SeMethods;

public class EnterDetails extends SeMethods  {
	public EnterDetails enterfname() {

		WebElement eleenterfname= locateElement("xpath","//input[@name='firstName']");
		type(eleenterfname,"vickyjaya");
		return this;

	}

	public schemePage viewMutualFund() {

		WebElement eleviewmutualfund= locateElement("xpath", "//a[text()='View Mutual Funds']");
		click(eleviewmutualfund);
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return new schemePage();
	}
	
}
