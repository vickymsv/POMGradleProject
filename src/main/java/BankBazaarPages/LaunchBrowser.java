package BankBazaarPages;


import wdMethods.SeMethods;

public class LaunchBrowser extends SeMethods  {

	public LandingPage lbrowser() {
		
		startApp("chrome", "https://www.bankbazaar.com/");
		return new LandingPage();
	}
}
