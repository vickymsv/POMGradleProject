package pages;

import org.openqa.selenium.WebElement;

import wdMethods.ProjectMethods;

public class ViewLeadPage extends ProjectMethods {

	public EditLead clickEditButton() {
		WebElement eleClickEdit =locateElement("xpath", "//a[contains(text(),'Edit')]");
		click(eleClickEdit);
		return new EditLead();
	}
	public MyLeadsPage clickDeleteButton() {
		WebElement eleClickDelete = locateElement("linktext", "Delete");
		click(eleClickDelete);
		return new MyLeadsPage();
	}
}
