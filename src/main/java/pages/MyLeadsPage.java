package pages;

import org.openqa.selenium.WebElement;

import wdMethods.ProjectMethods;

public class MyLeadsPage extends ProjectMethods{


	public CreateLeadPage clickCreateLead() {
		WebElement eleCreateLead = locateElement("linktext", "Create Lead");
		click(eleCreateLead);
		return new CreateLeadPage();
	}
	public FindLeadsPage clickFindLeads() {
		WebElement eleFindLeads = locateElement("xpath", "(//a[@href='/crmsfa/control/createLeadForm']/following::a)[1]");
		click(eleFindLeads);
		return new FindLeadsPage();
	}
	
}







