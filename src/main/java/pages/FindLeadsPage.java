package pages;

import org.openqa.selenium.WebElement;

import wdMethods.ProjectMethods;

public class FindLeadsPage extends ProjectMethods {

	static String text;
	public FindLeadsPage typeFirstname(String fname) {
		WebElement eleFirstname =locateElement("xpath", "(//input[@name='firstName'])[3]");
		type(eleFirstname,fname);
		return this;
	}
	public FindLeadsPage typeId() {
		WebElement eleTypeId = locateElement("name", "id");
		type(eleTypeId,text);
		return this;
	}
	
	public FindLeadsPage clickFindeads() throws InterruptedException {
		WebElement eleClickFindLeads =locateElement("xpath", "//button[contains(text(),'Find Leads')]");
		click(eleClickFindLeads);
		Thread.sleep(1000);
		return this;
	}
	public FindLeadsPage  clickEditTab() {
		WebElement eleClickEmailTab = locateElement("xpath", "//span[text()='Email']");
		click(eleClickEmailTab);
		return this;
	}
	
	public FindLeadsPage typeEmail(String email) {
		WebElement eleTypeEmail = locateElement("name", "emailAddress");
		type(eleTypeEmail,email);
		return this;
	}
	public FindLeadsPage getFirstIDText() {
		WebElement elegetFirstIDText = locateElement("xpath", "(//div[@class='x-grid3-cell-inner x-grid3-col-partyId'])[1]/a");
		 text = getText(elegetFirstIDText);
		 return this;
	}
	public ViewLeadPage clickFirstResult() {
		WebElement eleFirstResult =locateElement("xpath", "(//div[@class='x-grid3-cell-inner x-grid3-col-partyId'])[1]/a");
		click(eleFirstResult);
		return new ViewLeadPage();
	}
	public FindLeadsPage verifyText(String errormessage) {
		WebElement eleVerify = locateElement("class", "x-paging-info");
		verifyExactText(eleVerify, errormessage);
		return this;
	}
}
