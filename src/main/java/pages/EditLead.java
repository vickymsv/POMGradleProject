package pages;

import org.openqa.selenium.WebElement;

import wdMethods.ProjectMethods;

public class EditLead extends ProjectMethods {

	public EditLead updateIndustry(String industry ) {
		WebElement ele7 =locateElement("id", "updateLeadForm_industryEnumId");
		selectDropDownUsingText(ele7, industry);
		return this;
	}
	public ViewLeadPage clickUpdateButton() {
		WebElement eleUpdateButton =locateElement("xpath", "//input[@class='smallSubmit']");	
		click(eleUpdateButton);
		return new ViewLeadPage();
	}
}
